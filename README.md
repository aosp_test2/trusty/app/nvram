# Access-controlled NVRAM app for Trusty

This repository contains code to host the [reference implementation](https://android.googlesource.com/platform/system/nvram)
of Android's [Access-Controlled NVRAM HAL](https://android.googlesource.com/platform/hardware/libhardware/+/master/include/hardware/nvram.h)
in a Trusty app. The app exposes its service via a standard Trusty IPC endpoint,
accepting commands and producing replies in the [message format](https://android.googlesource.com/platform/system/nvram/+/master/messages/include/nvram/messages/nvram_messages.h)
defined by the reference implementation. The app relies on Trusty's secure
storage abstraction to store persistent NVRAM space contents and meta data.

*** note
**IMPORTANT NOTE**: The Access-Controlled NVRAM HAL spec calls for locked NVRAM
contents to be persisted "until the next full device reset". This means that
locked persistent NVRAM spaces must remain intact unless the user factory-resets
the entire device. In particular, NVRAM data may not be lost unless all other
device state gets wiped as well. The code in this repository **DOES NOT**
achieve this in its current state. To achieve compliance with the HAL spec, an
implementation can implement one of the following approaches:

* Use tamper-evident storage and enforce a full device wipe upon detecting
  tampering with NVRAM data. The code as is uses tamper-evident storage
  (as provided by STORAGE_CLIENT_TDEA_PORT), but Trusty's secure storage layer
  currently (1) can't distinguish tampering from initial setup and (2) doesn't
  indicate to the app when tampering has been detected.
* Use tamper-resistant storage (e.g. switch to STORAGE_CLIENT_TP_PORT) to enable
  secure storage to store NVRAM state in RPMB *and* make sure that NVRAM data
  gets wiped as part of hardware reset. This will generally require
  bootloader/recovery integration, which should either use an out-of-band
  approach to clear RPMB during hardware reset / recovery or send
  COMMAND_DISABLE_WIPE and COMMAND_WIPE_STORAGE to the Trusty NVRAM app at the
  appropriate points during boot and recovery, respectively.
***
